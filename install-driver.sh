#!/usr/bin/env bash

set -e
set -x

source .env

smartthings edge:drivers:package --channel="$CHANNEL_ID" --hub="$HUB_ID"
