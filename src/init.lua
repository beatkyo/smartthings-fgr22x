local ZwaveDriver = require "st.zwave.driver"
local capabilities = require "st.capabilities"
local defaults = require "st.zwave.defaults"
local log = require "log"

local ZWAVE_FINGERPRINTS = {{
    -- FGR221
    mfr = 0x010F,
    prod = 0X0300,
    model = 0x0102
}, {
    -- FGR223
    mfr = 0x010F,
    prod = 0X0300,
    model = 0x1000
}}

local function can_handle(opts, driver, device, ...)
    for _, fingerprint in ipairs(ZWAVE_FINGERPRINTS) do
        if device:id_match(fingerprint.mfr, fingerprint.prod, fingerprint.model) then
            return true
        end
    end
    return false
end

local driver_template = {
    NAME = "FGR22x",
    supported_capabilities = {capabilities.powerMeter, capabilities.refresh, capabilities.windowShade,
                              capabilities.windowShadeLevel},
    can_handle = can_handle
}

defaults.register_for_default_handlers(driver_template, driver_template.supported_capabilities)
local driver = ZwaveDriver("fgr-22x", driver_template)
driver:run()
